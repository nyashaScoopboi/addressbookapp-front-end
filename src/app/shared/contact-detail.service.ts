import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { ContactDetail } from './contact-detail.module';

@Injectable({
  providedIn: 'root'
})
export class ContactDetailService {

  constructor(private http:HttpClient) { }

  formData: ContactDetail = new ContactDetail();
  readonly baseURL = 'https://localhost:5001/api/ContactDetail';
  list: ContactDetail[];

  postContactDetail(){
    return this.http.post(this.baseURL, this.formData);
  }

  putContactDetail(){
    return this.http.put(`${this.baseURL}/${this.formData.contactId}`, this.formData);
  }

  deleteContactDetail(id:number){
    return this.http.delete(`${this.baseURL}/${id}`);
  }

  refreshList(){
    this.http.get(this.baseURL)
        .toPromise()
        .then(res => this.list = res as ContactDetail[]);
  }
}
