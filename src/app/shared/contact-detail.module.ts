export class ContactDetail {
  contactId: number=0;
  fullName: string='';
  email: string='';
  phone: string='';
  title: string='';
  company: string='';
  linkedIn: string='';
  skype: string='';
  tag: string='';
}
