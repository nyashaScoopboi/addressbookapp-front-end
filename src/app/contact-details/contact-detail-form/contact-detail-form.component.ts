import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactDetail } from 'src/app/shared/contact-detail.module';
import { ContactDetailService } from 'src/app/shared/contact-detail.service';

@Component({
  selector: 'app-contact-detail-form',
  templateUrl: './contact-detail-form.component.html',
  styles: [
  ]
})
export class ContactDetailFormComponent implements OnInit {

  constructor(public service: ContactDetailService) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    if (this.service.formData.contactId==0)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }

  insertRecord(form:NgForm){
    this.service.postContactDetail().subscribe(
      res => {
          this.resetForm(form);
          this.service.refreshList();
      },
      err => {console.log(err); }
    );
  }

  updateRecord(form: NgForm){
    this.service.putContactDetail().subscribe(
      res => {
          this.resetForm(form);
          this.service.refreshList();
      },
      err => {console.log(err); }
    );
  }

  resetForm(form:NgForm){
    form.form.reset();
    this.service.formData = new ContactDetail();
  }
}
