import { Component, OnInit } from '@angular/core';
import { ContactDetail } from '../shared/contact-detail.module';
import { ContactDetailService } from '../shared/contact-detail.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styles: [
  ]
})
export class ContactDetailsComponent implements OnInit {

  constructor(public service: ContactDetailService) { }

  ngOnInit(): void {
    this.service.refreshList();
  }

  populateForm(selectedRecord: ContactDetail){
    this.service.formData = Object.assign({}, selectedRecord);
  }

  onDelete(id:number){
    if(confirm('Delete Record?'))
      {
      this.service.deleteContactDetail(id)
      .subscribe(
        res=>{
          this.service.refreshList();
        },
        err=>{console.log(err)}
      )}
  }
}
