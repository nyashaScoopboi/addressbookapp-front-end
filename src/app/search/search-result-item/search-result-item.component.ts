import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';
import { ContactDetail } from 'src/app/shared/contact-detail.module';
import { ContactDetailService } from 'src/app/shared/contact-detail.service';

@Component({
  selector: 'app-search-result-item',
  templateUrl: './search-result-item.component.html',
  styleUrls: ['./search-result-item.component.scss'],
})
export class SearchResultItemComponent implements OnInit {
  constructor(public searchService: SearchService, public ContactDetailService: ContactDetailService, public ContactDetail: ContactDetail) {}

  ngOnInit(): void {}
}
