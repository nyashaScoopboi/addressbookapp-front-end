export interface SearchOption {
  contactId: number;
  fullName: string;
  email: string;
  phone: string;
  title: string;
  company: string;
  linkedIn: string;
  skype: string;
  tag: string;
}
